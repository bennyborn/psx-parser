# psx parser #
Gets game id and region for the given psx image.
Currently supports BIN, ISO, IMG, CDI and MDF (maybe even more).

## Info
Unfortunately PSX games do not contain any game titles. To get the title of a game you could use e.g. [http://ks23364.kimsufi.com/psxdata/databasepsx/](http://ks23364.kimsufi.com/psxdata/databasepsx/)