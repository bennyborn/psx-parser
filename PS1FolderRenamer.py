import os
import sys
from PS1Parser import PS1Parser


def main():

	os.chdir(r"D:\Downloads\Games\Sony\PlayStation1\rename_and_pack")

	for game in os.listdir("."):

		for file in os.listdir( game ):

			if file.lower().endswith( (".bin",".iso",".img",".cdi",".mdf",".nrg") ):

				try:

					ps1 = None
					ps1 = PS1Parser()

					ps1.parse( game + '/'+ file )
					newName = "%s [%s] [%s]" % (ps1.name,ps1.id,ps1.region)

					print "ren \"%s\" \"%s\"" % (game,newName) 
					print "move /-Y \"%s\" \"../pack/%s\"" % (newName,newName) 

				except:

					print "\n"
					print game
					print file
					print ps1.id
					print ps1.region

					raise

					sys.exit(0)
				
				break


if __name__ == "__main__":
    main()