from pyquery import PyQuery as pq
from lxml import etree
import urllib
import urllib2
import re
import sqlite3
import sys, traceback

site = 'http://ks23364.kimsufi.com/psxdata/databasepsx/'
conn = sqlite3.connect('ps1.db')

def parseGameData( url ):

	data = {
		'title':'',
		'serials':'',
		'region':'',
		}

	page = pq(site+url)
	toc = page('#table4')

	# fallback: sometimes pyquery can't parse the page
	# get the raw source via urllib instead
	if len(toc) <= 0:
		response = urllib2.urlopen(site+url)
		page = pq(response.read())
		toc = page('#table4')


	data['title'] = toc('tr:eq(0) td:eq(1)').html().strip()
	data['serials'] = parseSerials( toc('tr:eq(2) td:eq(1)').html() )
	data['region'] = toc('tr:eq(3) td:eq(1)').html().strip()

	return data

def parseSerials( str ):
	str = str.strip().replace(' ','')
	serials = filter(None, re.compile('[&|,|<br/>]').split( str ))
	return serials


# print parseGameData('/games/P/R/SCES-01706.html')
#print parseGameData('/games/U/C/SLUS-01041.html')
#sys.exit(0)


#d = pq(site+'ulist.html')
#d = pq(site+'plist.html')
d = pq(site+'jlist.html')

links = d('a:contains("INFO")')

fail = 0
complete = 0

for a in links:

	c = conn.cursor()

	# parse serials to check for duplicates
	serials = parseSerials( pq(a).parent().parent().find('td.col2').html() )

	alreadyFetched = False

	# check if games with given serial(s) is already in db
	for serial in serials:
		query = ("SELECT COUNT(*) FROM games WHERE serials LIKE '%s%%' OR serials LIKE '%%%s%%' OR serials LIKE '%%%s';" % (serial,serial,serial))
		c.execute( query )
		result = c.fetchone()

		if result[0] > 0:
			alreadyFetched = True

	# not in db? parse game data
	if not alreadyFetched:

		try:

			data = parseGameData( pq(a).attr('href') )

			try:
				val = (data['title'],data['region'],','.join(data['serials']))
				cmd = "INSERT INTO games (title,region,serials) VALUES (?,?,?)"
				c.execute(cmd, val)

				conn.commit()
			except sqlite3.IntegrityError:
				pass

			print data
			complete = complete+1

		except:
			print site+pq(a).attr('href')
			#traceback.print_exc(file=sys.stdout)
			#print pq( site+pq(a).attr('href') ).html().encode('utf-8')
			#sys.exit(0)
			fail = fail+1
			pass

	#else:
		#print "skipping %s" % serials

	#break

conn.close()
print ('%s %d / %d %s') % ('FAILED TO PARSE ',fail, complete, 'SUCCESSFULL')