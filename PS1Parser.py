import os
import string
import re
import sqlite3

scriptPath = os.path.dirname(os.path.realpath(__file__))

class PS1Parser:

	def __init__(self):
		self.id = ""
		self.name = ""
		self.region = ""
		self.regions = { 
			"SLUS":"NTSC-U", 
			"SCUS":"NTSC-U", 
			"SLES":"PAL", 
			"SCES":"PAL", 
			"SCED":"PAL", 
			"SLPS":"NTSC-J", 
			"SLPM":"NTSC-J", 
			"SIPS":"NTSC-J", 
			"CPCS":"NTSC-J", 
			"SCPS":"NTSC-J", 
			"ESPM":"NTSC-J",
			"PAPX":"NTSC-J",
			"PBPX":"NTSC-U"
			}

	def parse(self,iso_name):

		global scriptPath

		iso = open( iso_name,"rb" )
		data = str( iso.read() )
		iso.close()

		# find position of game id
		m_id = re.search(r"BOOT(2|)(\s+|)=(\s+|)cdrom(0|):(\\*)(.*?)(\x3B|\x0D)", data)
		matches = len(m_id.groups())

		if matches == "1":
			raise Exception("ID not found")

		# re-format game id
		self.id = m_id.group(6).replace("_","-").replace(".","").upper().strip()[-10:]

		#print self.id

		# get region
		self.region = self.regions[ self.id[:4] ]

		# try to get better game title from local database
		conn = sqlite3.connect(scriptPath+'/ps1.db')
		c = conn.cursor()

		query = ("SELECT title FROM games WHERE serials LIKE '%%%s%%' OR serials = '%s' OR serials LIKE '%%%s%%' LIMIT 1;" % (self.id,self.id,self.id))
		c.execute( query )
		result = c.fetchone()

		if len(result[0]):

			name = result[0]

			# remove characters forbidden in windows filenames
			for char in '\\/:*?"<>|':
				name = name.replace(char,'')

			name = name.replace('  ',' ').strip()
			self.name = name

		conn.close()

		return None


def main():

	os.chdir(r"D:\Downloads\Games\Sony\PlayStation1\rename_and_pack\Hardware_online_arena_pal")

	for files in os.listdir("."):
	    if files.lower().endswith( (".bin",".iso",".img",".cdi",".mdf") ):

			ps1 = None
			ps1 = PS1Parser()

			ps1.parse(files)

			print "ren \"%s\" \"%s [%s] [%s].iso\"" % (files,ps1.name,ps1.id,ps1.region)


if __name__ == "__main__":
    main()